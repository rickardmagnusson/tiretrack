import React, { Component } from 'react';
import './App.css';

import $ from 'jquery';
window.jQuery = $;
window.$ = $;

const jQuery = $;




const updateList = function () {
    $.get("https://remote.bootproject.se/api/values", { "dim1": $(".step1").val(), "dim2": $(".step2").val(), "dim3": $(".step3").val(), "valSasong": $(".step4").val(), "valBilTyp": $(".step5").val() }, function (data) {
        $("#tirelist").empty();
        $.each(data.results, function (i, item) {
            $("#tirelist").append("<dt><img style=\"height: 100px;\" src=http://www.dackgrossisten.se" + data.results[i].image + "  />" + data.results[i].name + "</dt><dd>" + data.results[i].price + "<button>Best&auml;ll</button></dd>")
        })
    });
}

$('#tires').cascadingDropdown({
    textKey: 'key',
    valueKey: 'value',


    selectBoxes: [
        {
            selector: '.step1',
            paramName: 'dim1',
            onChange: function (value) {
                updateList();
            }
        },
        {
            selector: '.step2',
            requires: ['.step1'],
            paramName: 'dim2',
            url: 'https://remote.bootproject.se/api/values/profile',
            valueKey: 'value',
            onChange: function (value) {
                updateList();
            }
        },
        {
            selector: '.step3',
            requires: ['.step1', '.step2'],
            paramName: 'dim2',
            url: 'https://remote.bootproject.se/api/values/size',
            valueKey: 'value',
            onChange: function (value) {
                updateList();
            }
        },
        {
            selector: '.step4',
            paramName: 'valSasong',
            valueKey: 'key',
            onChange: function (value) {
                updateList();
            }
        },
        {
            selector: '.step5',
            paramName: 'valBilTyp',
            requireAll: false,
            onChange: function (value) {
                updateList();
            }
        }
    ]
});


class App extends Component {

   

  render() {
    return (
      <div className="container">
        <div className="row">
            <div className="col-sm-12">
                <h2>Tire rippoff app</h2>
                <div id="tires">
                    <select className="step1" name="dim1">
                        <option value="">[Bredd]</option>
                        <option>135</option>
                        <option>145</option>
                        <option>155</option>
                        <option>165</option>
                        <option>175</option>
                        <option>185</option>
                        <option>195</option>
                        <option>205</option>
                        <option>215</option>
                        <option>225</option>
                        <option>235</option>
                        <option>245</option>
                        <option>255</option>
                        <option>265</option>
                        <option>275</option>
                        <option>285</option>
                        <option>295</option>
                        <option>30</option>
                        <option>305</option>
                        <option>31</option>
                        <option>315</option>
                        <option>32</option>
                        <option>325</option>
                        <option>33</option>
                        <option>345</option>
                        <option>35</option>
                        <option>37</option>
                        <option>40</option>
                    </select>

                    <select className="step2" name="dim2">
                        <option value="">[Profil]</option>
                    </select>

                    <select className="step3" name="dim3">
                        <option value="">[Tum]</option>
                    </select>

                    <select className="step4" name="valSasong">
                        <option value="1">Sommar</option>
                        <option value="4">Friktion</option>
                        <option value="5">Dubb</option>
                    </select>

                    <select className="step5" name="valBilTyp">
                        <option value="1">Personbil</option>
                        <option value="2">C Däck</option>
                    </select>
                </div>

                <dl id="tirelist"></dl>

            </div>
        </div>
      </div>
    );
  }
}

export default App;
