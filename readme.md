﻿# Bootproject on HtmlAgility 

*This project contains an example and tests on how to build and create HtmlAgility components.*

It actually rips another websites content, and creates a service API from the data in that page.

See the result of the project on the [Project page](https://ws.bootproject.se/) 

![A real site implementation](assets/dv.png "Real site")
