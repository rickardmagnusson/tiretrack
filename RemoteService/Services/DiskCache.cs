﻿using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace RemoteService.Services
{
    public class DiskCache
    {


        private string appRootFolder;
        public DiskCache(IHostingEnvironment env)
        {
            appRootFolder = env.ContentRootPath;
        }



        private async void CreateCacheItem(object model, string fileName)
        {
            var cachefile = appRootFolder + Path.DirectorySeparatorChar.ToString() + "wwwroot" + Path.DirectorySeparatorChar.ToString() + "cache" + Path.DirectorySeparatorChar.ToString() + fileName;

            using (StreamWriter s = File.CreateText(cachefile))
            {
                await s.WriteAsync(JsonConvert.SerializeObject(model));
            }
        }

        public T Get<T>(string fileName) where T : class
        {
            var cachefile = appRootFolder + Path.DirectorySeparatorChar.ToString() + "wwwroot" + Path.DirectorySeparatorChar.ToString() + "cache" + Path.DirectorySeparatorChar.ToString() + fileName;
            if (File.Exists(cachefile))
                return JsonConvert.DeserializeObject<T>(File.OpenText(cachefile).ReadToEnd());
            else
                return null;
        }



        public T Set<T>(object model, string fileName) where T : class
        {
            var cachefile = appRootFolder + Path.DirectorySeparatorChar.ToString() + "wwwroot" + Path.DirectorySeparatorChar.ToString() + "cache" + Path.DirectorySeparatorChar.ToString() + fileName;

            if (File.Exists(cachefile))
                return JsonConvert.DeserializeObject<T>(File.OpenText(cachefile).ReadToEnd());
            else
                CreateCacheItem(model, fileName);

            var o = JsonConvert.SerializeObject(model);
            return JsonConvert.DeserializeObject<T>(o);
        }
    }
}
