﻿using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.DotNet.PlatformAbstractions;
using Microsoft.Extensions.PlatformAbstractions;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;

namespace RemoteService.Services
{

    public static class CacheExtensions
    {
        public static IServiceCollection AddCache(this IServiceCollection services, IHostingEnvironment env, IConfiguration configuration)
        {
            services.AddSingleton<IHostingEnvironment>(env);
            return services.AddTransient<ICacheService, CacheService>();
        }
    }


    public interface ICacheService
    {
        Task<T> Get<T>(object model, string fileName) where T : class;
    }


    public class CacheService : ICacheService
    {
        private readonly IHostingEnvironment hostingEnvironment;
        private string appRootFolder;

        public CacheService(IHostingEnvironment env)
        {
            hostingEnvironment = env;
            appRootFolder = env.ContentRootPath;
        }


        private async void CreateCacheItem(object model, string fileName)
        {
            var cachefile = appRootFolder + Path.DirectorySeparatorChar.ToString() + "wwwroot" + Path.DirectorySeparatorChar.ToString() + "cache" + Path.DirectorySeparatorChar.ToString() + fileName;
           
            using (StreamWriter s = File.CreateText(cachefile))
            {
                await s.WriteAsync(JsonConvert.SerializeObject(model));
            }
        }

        public async Task<T> Get<T>(object model, string fileName) where T : class
        {
            var cachefile = appRootFolder + Path.DirectorySeparatorChar.ToString() + "wwwroot" + Path.DirectorySeparatorChar.ToString() + "cache" + Path.DirectorySeparatorChar.ToString() + fileName;

            if (File.Exists(cachefile))
                return await Task.Run(() => JsonConvert.DeserializeObject<T>(File.OpenText(cachefile).ReadToEnd()));
            else
                await Task.Run(() =>  CreateCacheItem(model, fileName));

            var o = JsonConvert.SerializeObject(model);
            return await Task.Run(() =>  JsonConvert.DeserializeObject<T>(o));
        }

    }
}
