/*
  Author Bootproject 2018
 (Rickard Magnusson)
 */

//
//const host = "http://localhost:61070";
const host = "https://ws.bootproject.se";
const imageurl = "http://www.dackgrossisten.se/Images/";

function urlExists(url) {
    $.ajax({
        type: 'HEAD',
        url: url,
        success: function () {
            return true;
        },
        error: function () {
            return false;
        }
    });
}


const updateList = function () {
    $.get(host + "/api/values", { "dim1": $(".step1").val(), "dim2": $(".step2").val(), "dim3": $(".step3").val(), "valSasong": $(".step4").val(), "valBilTyp": $(".step5").val() }, function (data) {
        $("#tirelist").empty();
        if (data !== undefined) {
            $.each(data.results, function (i, item) {

                //Failsave load of image.
                var img = "/img/" + data.results[i].image.replace("%20", "-"); //urlExists("/img/" + data.results[i].image.replace("%20","-")) ? (host + "/img/" + data.results[i].image) : (imageurl +""+ data.results[i].image);

                $("#tirelist").append(
                    "<dl><dt>" + data.results[i].name +
                    "</dt><dd><img style=\"height: 100px;\" src=\""
                    + img + "\" /><div class=\"tag\">"
                    + data.results[i].price + " &nbsp;<input class=\"inp\" type=\"tel\" size=\"2\" min=\"0\" max=\"18\" value=\"4\" pattern=\"[0-9]*\" />&nbsp;<a class=\"button\">Best&auml;ll</a></div></dd></dl>")
            });
        }
    });
};

$('#tires').cascadingDropdown({
    textKey: 'key',
    valueKey: 'value',

    selectBoxes: [
        {
            selector: '.step1',
            paramName: 'dim1',
            onChange: function (value) {
                updateList();
            }
        },
        {
            selector: '.step2',
            requires: ['.step1'],
            paramName: 'dim2',
            url: host+ '/api/values/profile',
            valueKey: 'value',
            onChange: function (value) {
                updateList();
            }
        },
        {
            selector: '.step3',
            requires: ['.step1', '.step2'],
            paramName: 'dim2',
            url: host +'/api/values/size',
            valueKey: 'value',
            onChange: function (value) {
                updateList();
            }
        },
        {
            selector: '.step4',
            paramName: 'valSasong',
            valueKey: 'key',
            onChange: function (value) {
                updateList();
            }
        },
        {
            selector: '.step5',
            paramName: 'valBilTyp',
            requireAll: false,
            onChange: function (value) {
                updateList();
            }
        }
    ]
});