(function ($, undefined) {
    'use strict';

    // constructor
    function Dropdown(options, parent) {
        this.el = $(options.selector, parent);
        this.options = $.extend({}, parent.options, options);
        this.requiredDropdowns = options.requires && options.requires.length ? $(options.requires.join(','), parent) : null;
        this.requirementsMet = true;
        this.originalOptions = this.el.children('option');
        this.init();
    }

    // methods
    Dropdown.prototype = {
        init: function () {
            var self = this;

            if (typeof self.options.onChange === 'function') {
                self.el.on('change', function () {
                    self.options.onChange.call(self, self.el.val());
                });
            }

            if (self.requiredDropdowns) {
                self.requiredDropdowns.on('change', function () {
                    self.checkRequirements();
                });
            }

            self.checkRequirements();
        },

        enable: function () {
            this.el.removeAttr('disabled');
        },

        disable: function () {
            this.el.attr('disabled', 'disabled');
        },

        checkRequirements: function () {
            var self = this;

            if (self.requiredDropdowns) {
                if (self.options.requireAll) {
                    self.requirementsMet = self.requiredDropdowns.filter(function () {
                        return !!$(this).val();
                    }).length == self.options.requires.length;
                } else {
                    self.requirementsMet = self.requiredDropdowns.filter(function () {
                        return !!$(this).val();
                    }).length > 0;
                }
            }

            self.disable();
            if (self.requirementsMet) {
                self.fetchList(function () {
                    self.enable();
                });
            }
        },

        fetchList: function (callback) {
            var self = this;

            if (!self.options.url) {
                typeof callback === 'function' && callback();
                return;
            }

            if (!self.options.textKey || !self.options.valueKey) {
                $.error('Insufficient parameters');
            }

            self.el.children('option').remove();
            self.el.append(self.originalOptions);

            var ajaxData = {};

            if (self.requiredDropdowns) {
                $.each(self.requiredDropdowns, function () {
                    var instance = $(this).data('plugin_cascadingDropdown');
                    if (instance.options.paramName) {
                        ajaxData[instance.options.paramName] = instance.el.val();
                    }
                });
            }

            $.ajax({
                url: self.options.url,
                data: self.options.useJson ? JSON.stringify(ajaxData) : ajaxData,
                dataType: self.options.useJson ? 'json' : undefined,
                type: self.options.usePost ? 'post' : 'get',
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (!data) {
                        return;
                    }

                    // For .NET web services
                    if (data.hasOwnProperty('d')) {
                        data = data.d;
                    }

                    data = typeof data === 'string' ? $.parseJSON(data) : data;
                    $.each(data, function (index, item) {
                        if (!item[self.options.textKey] || !item[self.options.valueKey]) {
                            return true;
                        }

                        var defaultAttr = '';
                        if (self.options.defaultValue == item[self.options.valueKey]) {
                            defaultAttr = ' selected="selected"';
                        }

                        self.el.append('<option value="' + item[self.options.valueKey] + '"' + defaultAttr + '>' + item[self.options.textKey] + '</option>');
                    });

                    typeof callback === 'function' && callback();
                }
            });
        }
    };

    // jQuery plugin declaration
    $.fn.cascadingDropdown = function (options) {
        return this.each(function () {
            var parent = this;
            parent.options = options;
            $.each(options.selectBoxes, function () {
                $(this.selector, parent).data('plugin_cascadingDropdown', new Dropdown(this, parent));
            });
        });
    };
})(jQuery);
/*
  Author Bootproject 2018
 (Rickard Magnusson)
 */

//
//const host = "http://localhost:61070";
const host = "https://ws.bootproject.se";
const imageurl = "http://www.dackgrossisten.se/Images/";

function urlExists(url) {
    $.ajax({
        type: 'HEAD',
        url: url,
        success: function () {
            return true;
        },
        error: function () {
            return false;
        }
    });
}


const updateList = function () {
    $.get(host + "/api/values", { "dim1": $(".step1").val(), "dim2": $(".step2").val(), "dim3": $(".step3").val(), "valSasong": $(".step4").val(), "valBilTyp": $(".step5").val() }, function (data) {
        $("#tirelist").empty();
        if (data !== undefined) {
            $.each(data.results, function (i, item) {

                //Failsave load of image.
                var img = "/img/" + data.results[i].image.replace("%20", "-"); //urlExists("/img/" + data.results[i].image.replace("%20","-")) ? (host + "/img/" + data.results[i].image) : (imageurl +""+ data.results[i].image);

                $("#tirelist").append(
                    "<dl><dt>" + data.results[i].name +
                    "</dt><dd><img style=\"height: 100px;\" src=\""
                    + img + "\" /><div class=\"tag\">"
                    + data.results[i].price + " &nbsp;<input class=\"inp\" type=\"tel\" size=\"2\" min=\"0\" max=\"18\" value=\"4\" pattern=\"[0-9]*\" />&nbsp;<a class=\"button\">Best&auml;ll</a></div></dd></dl>")
            });
        }
    });
};

$('#tires').cascadingDropdown({
    textKey: 'key',
    valueKey: 'value',

    selectBoxes: [
        {
            selector: '.step1',
            paramName: 'dim1',
            onChange: function (value) {
                updateList();
            }
        },
        {
            selector: '.step2',
            requires: ['.step1'],
            paramName: 'dim2',
            url: host+ '/api/values/profile',
            valueKey: 'value',
            onChange: function (value) {
                updateList();
            }
        },
        {
            selector: '.step3',
            requires: ['.step1', '.step2'],
            paramName: 'dim2',
            url: host +'/api/values/size',
            valueKey: 'value',
            onChange: function (value) {
                updateList();
            }
        },
        {
            selector: '.step4',
            paramName: 'valSasong',
            valueKey: 'key',
            onChange: function (value) {
                updateList();
            }
        },
        {
            selector: '.step5',
            paramName: 'valBilTyp',
            requireAll: false,
            onChange: function (value) {
                updateList();
            }
        }
    ]
});