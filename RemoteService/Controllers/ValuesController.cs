﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Net.Http;
using System.Xml;
using System.IO;
using HtmlAgilityPack;
using System.Text;
using System.Text.RegularExpressions;
using RemoteService.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace RemoteService.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {       
        private readonly DiskCache _cache;
        private readonly string _host;



        public ValuesController(IHostingEnvironment env, IConfiguration configuration)
        {
            _cache = new DiskCache(env);
            _host = configuration["Environment:Domain"];
        }

        //[Route("/api/refresh")]
        ////Need to refresh cached files regulay to avoid wrong parameters.
        ////Need to scedule this. TODO!!
        //public async Task<dynamic> RefreshCache() {

        //    var hc = new HttpClient();
        //    var result = await hc.GetAsync(_host + "/Dack/searchDack");
        //    var stream = await result.Content.ReadAsStreamAsync();
        //    var doc = new HtmlDocument();

        //    doc.Load(stream);

        //    var node = doc.GetElementbyId("dim1"); //Bredd

           
        //    List<string> vals = new List<string>();

        //    foreach (var item in node.SelectNodes("option"))
        //    {
        //        vals.Add(Parser.ParseNode(item));
        //    }


        //    return await Task.FromResult(vals);
        //}



        [Route("/api/values")]
        public async Task<dynamic> Get(string dim1="", string dim2 = "", string dim3 = "", string valSasong = "1", string valBilTyp = "1")
        {
            string param = $"/Dack/searchDack?dim1={dim1}&dim2={dim2}&dim3={dim3}&valSasong={valSasong}&valBilTyp={valBilTyp}";
            var par = $"{dim1}{dim2}{dim3}{valSasong}{valBilTyp}";
            var url = _host + param;
        
            object c = _cache.Get<Model>(par);
            if (c != null)
                return c;

            var hc = new HttpClient();
            var result = await hc.GetAsync(url);
            var stream = await result.Content.ReadAsStreamAsync();
            var doc = new HtmlDocument();

            doc.Load(stream);

            var nodes = doc.DocumentNode.SelectNodes("//td//select");
            var inputs = new List<string>();

            foreach (var node in nodes){
                inputs.Add(Parser.ParseNode(node));
            }

            var table = doc.GetElementbyId("articles");
           
            var data = table.SelectNodes("tr") != null ? Parser.ParseTable(table) : new List<Row>();

            var model = new Model()
            {
                dim1 = inputs[0],
                dim2 = inputs[1],
                dim3 = inputs[2],
                valSasong = inputs[3],
                valBilTyp = inputs[4],
                Results = data
            };

            if (!string.IsNullOrEmpty(dim3))
            {
                if (_cache.Get<Model>(par) == null) { 
                    model.InCache = true;
                    _cache.Set<Model>(model, par);
                    
                }
                model.InCache = false;
                return model; //does not reload properly.!!! Cant return from cache yet.
            }

            return await Task.Run(() => _cache.Get<Model>(url));
        }




        [Route("/api/values/profile")]
        public async Task<List<Item>> Profile(string dim1 = "1")
        {
            var profiles = new List<Item>();

            if (!string.IsNullOrEmpty(dim1))
            {
                var hc = new HttpClient();
                var result = await hc.GetAsync($"{_host}/Dack/searchDack?dim1={dim1}");
                var stream = await result.Content.ReadAsStreamAsync();
                var doc = new HtmlDocument();

                doc.Load(stream);

                var nodes = doc.GetElementbyId("dim2"); //Profil
                var dim2 = nodes.SelectNodes("option");

                dim2.ToList().ForEach(c=> {
                    profiles.Add(new Item { Key = c.InnerText, Value = c.InnerText });
                });

                profiles.RemoveAt(0);
            }

            return await Task.FromResult(profiles);
        }




        [Route("/api/values/size")]
        public async Task<List<Item>> Size(string dim1="1", string dim2 = "1")
        {
            var profiles = new List<Item>();

            if (!string.IsNullOrEmpty(dim2))
            {
                var hc = new HttpClient();
                var result = await hc.GetAsync($"{_host}/Dack/searchDack?dim1={dim1}&dim2={dim2}");
                var stream = await result.Content.ReadAsStreamAsync();
                var doc = new HtmlDocument();

                doc.Load(stream);

                var nodes = doc.GetElementbyId("dim3"); //Tum
                var dim = nodes.SelectNodes("option");

                dim.ToList().ForEach(c => {
                    profiles.Add(new Item { Key = c.InnerText, Value = c.InnerText });
                });

                profiles.RemoveAt(0);
            }

            return await Task.FromResult(profiles);
        }




        public class Item
        {
            public string Key { get; set; }
            public string Value { get; set; }
        }




        public class Model
        {
            public string dim1 { get; set; } = ""; //Bredd
            public string dim2 { get; set; } = ""; //Profil
            public string dim3 { get; set; } = ""; //Tum
            public List<string> dim2List { get; set; } = new List<string>();
            public List<string> dim3List { get; set; } = new List<string>();
            public string valSasong { get; set; } = ""; //Säsong
            public string valBilTyp { get; set; } = ""; //Biltyp
            public List<Row> Results { get; set; } = new List<Row>();
            public bool InCache { get; set; } = false;
        }
    }




    public class Row
    {
        public string Name { get; set; } = "";
        public string Image { get; set; } = "";
        public string Price { get; set; } = "";
    }




    public static class Parser
    {
        public static List<Row> ParseTable(HtmlNode table)
        {
            var Rows = new List<Row>();

            foreach (HtmlNode row in table.SelectNodes("tr"))
            {
                var newrow = new Row();
                var td1 = row.SelectNodes("td")[1];
                var td2 = row.SelectNodes("td")[0];
                var td3 = row.SelectNodes("td")[3];
                var text = td1.InnerText.Trim().Replace($"\r\n", "").Replace("&nbsp;", "");
                var matches = Regex.Matches(text, @"\w+[^\s]*\w+|\w"); //Removes any space

                var name = string.Empty;
                foreach (Match match in matches){ name += " " +  match.Value; }

                newrow.Name = name;
                newrow.Image = ExtractImage(td2);
                newrow.Price = td3.InnerText.Trim().Replace($"\r\n", "").Replace("&nbsp;", "").TrimStart().TrimEnd();
                Rows.Add(newrow);
            }

            return Rows;
        }




        public static string ExtractImage(HtmlNode node)
        {
            if (node.ChildNodes.FindFirst("img") == null)
                return string.Empty;

            var img = node.ChildNodes.FindFirst("img").Attributes["src"].Value.Replace(" ", "%20").Replace("/Images/", "");
            CacheImage(img);
            
            return img;
        }



        public static async void CacheImage(string url)
        {
            var img = "http://www.dackgrossisten.se/Images/" + url;
            var filename = url.Replace("/Images/", "");

            using (var client = new HttpClient())
            {
                using (var result = await client.GetAsync(img))
                {
                    if (result.IsSuccessStatusCode)
                    {
                        var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "img", filename.Replace("%20","-"));
                        var stream = new FileStream(path, FileMode.Create);
                        await Task.Run(() => result.Content.CopyToAsync(stream));
                    }

                }
            }
        }




        public static string ParseNode(HtmlNode node)
        {
                try {
                    bool selected = false; 
                    foreach (var item in node.SelectNodes("option"))
                    {
                        if (item.Attributes["selected"] != null)
                        {
                            selected = true;
                            return  item.InnerText;
                        }
                            
                    }
                    if (!selected)
                    {
                        return  node.Attributes["value"].Value;
                    }
                }
                catch
                {
                    return  "Node ERR";
                }

            return node.Attributes["name"].Value;
        }
    }
}
