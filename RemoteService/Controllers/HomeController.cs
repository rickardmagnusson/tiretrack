﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace RemoteService.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }



    public class ViewModel
    {
        public IEnumerable<Item> Items{ get; set; }
    }


    public class Item
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}